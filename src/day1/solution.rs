use std::{default, io::BufRead, u32};

use crate::{
    fs_util,
    mode::{self, RunMode},
    parts::Parts,
};

const POSSIBLE_STR_DIGITS: [&str; 9] = [
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

fn parse_digits(subject: &str) -> u32 {
    let mut start = 0;
    let mut end = 0;
    for n in subject.chars() {
        if n.is_ascii_digit() {
            start = n.to_digit(10).unwrap();
            break;
        }
    }

    for n in subject.chars().rev() {
        if n.is_ascii_digit() {
            end = n.to_digit(10).unwrap();
            break;
        }
    }

    return (10 * start) + end;
}

fn parse_string_digits(input: &str, digits: &mut Vec<u32>) -> u32 {
    let mut subject: &str = &input[0..];
    let mut first_digit: i32 = -1;
    let mut second_digit: i32 = -1;

    let mut count = 0;
    loop {
        if count >= input.len() {
            break;
        }

        for str_digit in POSSIBLE_STR_DIGITS.iter() {
            if subject.starts_with(*str_digit) {
                let index = POSSIBLE_STR_DIGITS
                    .iter()
                    .position(|&r| r == *str_digit)
                    .unwrap();
                first_digit = (index + 1) as i32;
                break;
            } else {
                let maybe_first_digit = subject.chars().next().unwrap();
                if maybe_first_digit.is_ascii_digit() {
                    first_digit = maybe_first_digit.to_digit(10).unwrap() as i32;
                    break;
                }
            }
        }

        if first_digit > 0 {
            break;
        }

        count += 1;

        subject = &subject[1..]
    }

    count = 0;
    loop {
        if count >= input.len() {
            break;
        }
        for str_digit in POSSIBLE_STR_DIGITS.iter().rev() {
            if subject.ends_with(*str_digit) {
                let index = POSSIBLE_STR_DIGITS
                    .iter()
                    .position(|&r| r == *str_digit)
                    .unwrap();
                second_digit = (index + 1) as i32;
                break;
            } else {
                let maybe_first_digit = subject.chars().rev().next().unwrap();
                if maybe_first_digit.is_ascii_digit() {
                    second_digit = maybe_first_digit.to_digit(10).unwrap() as i32;
                    break;
                }
            }
        }

        if second_digit > 0 {
            break;
        }
        count += 1;
        subject = &subject[0..&subject.len() - 1]
    }

    return (10 * first_digit + second_digit) as u32;
}

pub fn run(mode: RunMode, part: Parts) {
    let fp;
    match part {
        Parts::Part1 => {
            match mode {
                RunMode::Example => (fp = "./src/day1/example_part1.txt"),
                RunMode::TestCase => (fp = "./src/day1/test_part1.txt"),
            }
            if let Ok(lines) = fs_util::read_lines(fp) {
                let res = lines
                    .map(|line| parse_digits(line.unwrap().as_str()))
                    .sum::<u32>();
                println!("{}", res)
            }
        }
        Parts::Part2 => {
            match mode {
                RunMode::Example => (fp = "./src/day1/example_part2.txt"),
                RunMode::TestCase => (fp = "./src/day1/test_part1.txt"), // Test case same as Part
                                                                         // 1
            }
            if let Ok(lines) = fs_util::read_lines(fp) {
                let res = lines
                    .filter(|line| !line.as_ref().unwrap().as_str().is_empty())
                    .map(|line| {
                        return parse_string_digits(line.as_ref().unwrap().as_str(), &mut vec![]);
                    })
                    .sum::<u32>();

                println!("{}", res)
            }
        }
    }
}

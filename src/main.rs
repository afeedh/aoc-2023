mod day1;
mod day2;
mod day3;
pub mod fs_util;
pub mod mode;
pub mod parts;
pub fn main() {
    day3::solution::run(mode::RunMode::TestCase, parts::Parts::Part1);
}

use std::{char, default, error::Error, io::BufRead, ops::Index, u32, usize, vec};

use crate::{fs_util, mode::RunMode, parts::Parts};

fn is_within_bounds(row_to_check: i32, col_to_check: i32, matrix: &Vec<Vec<char>>) -> bool {
    return row_to_check >= 0
        && (row_to_check as usize) < matrix.len()
        && col_to_check >= 0
        && (col_to_check as usize) < matrix[0].len();
}

fn neighbour_has_special_symbol(matrix: &Vec<Vec<char>>, i: usize, z: i32) -> bool {
    let mut found = false;
    // up
    let mut row_to_check: i32 = i as i32 - 1;
    let mut col_to_check: i32 = z as i32;
    if !found && is_within_bounds(row_to_check, col_to_check, matrix) {
        let el_to_check: char = matrix[row_to_check as usize][col_to_check as usize];
        if !el_to_check.is_ascii_digit() && el_to_check != '.' {
            found = true;
        }
    }

    // down
    let mut row_to_check: i32 = i as i32 + 1;
    let mut col_to_check: i32 = z as i32;
    if !found && is_within_bounds(row_to_check, col_to_check, matrix) {
        let el_to_check: char = matrix[row_to_check as usize][col_to_check as usize];
        if !el_to_check.is_ascii_digit() && el_to_check != '.' {
            found = true;
        }
    }

    // left
    let mut row_to_check: i32 = i as i32;
    let mut col_to_check: i32 = z as i32 - 1;
    if !found && is_within_bounds(row_to_check, col_to_check, matrix) {
        let el_to_check: char = matrix[row_to_check as usize][col_to_check as usize];
        if !el_to_check.is_ascii_digit() && el_to_check != '.' {
            found = true;
        }
    }

    // right
    let mut row_to_check: i32 = i as i32;
    let mut col_to_check: i32 = z as i32 + 1;
    if !found && is_within_bounds(row_to_check, col_to_check, matrix) {
        let el_to_check: char = matrix[row_to_check as usize][col_to_check as usize];
        if !el_to_check.is_ascii_digit() && el_to_check != '.' {
            found = true;
        }
    }

    // diag left up
    let mut row_to_check: i32 = i as i32 - 1;
    let mut col_to_check: i32 = z as i32 - 1;
    if !found && is_within_bounds(row_to_check, col_to_check, matrix) {
        let el_to_check: char = matrix[row_to_check as usize][col_to_check as usize];
        if !el_to_check.is_ascii_digit() && el_to_check != '.' {
            found = true;
        }
    }

    // diag right up
    let mut row_to_check: i32 = i as i32 - 1;
    let mut col_to_check: i32 = z as i32 + 1;
    if !found && is_within_bounds(row_to_check, col_to_check, matrix) {
        let el_to_check: char = matrix[row_to_check as usize][col_to_check as usize];
        if !el_to_check.is_ascii_digit() && el_to_check != '.' {
            found = true;
        }
    }
    // diag left down
    let mut row_to_check: i32 = i as i32 + 1;
    let mut col_to_check: i32 = z as i32 - 1;
    if !found && is_within_bounds(row_to_check, col_to_check, matrix) {
        let el_to_check: char = matrix[row_to_check as usize][col_to_check as usize];
        if !el_to_check.is_ascii_digit() && el_to_check != '.' {
            found = true;
        }
    }

    // diag right down
    //
    let mut row_to_check: i32 = i as i32 + 1;
    let mut col_to_check: i32 = z as i32 + 1;
    if !found && is_within_bounds(row_to_check, col_to_check, matrix) {
        let el_to_check: char = matrix[row_to_check as usize][col_to_check as usize];
        if !el_to_check.is_ascii_digit() && el_to_check != '.' {
            found = true;
        }
    }
    return found;
}

pub fn part1(matrix: &Vec<Vec<char>>) -> i32 {
    let mut res: i32 = 0;
    for (i, row) in matrix.iter().enumerate() {
        let mut j = 0;
        let mut el = row[j];

        let mut start_idx: i32 = 0;
        let mut end_idx: i32 = -1;

        loop {
            if j >= row.len() {
                break;
            }

            el = row[j];

            if el.is_ascii_digit() {
                start_idx = j as i32;
                end_idx = -1;

                let mut reached_end = true;
                // find the end_idx
                for (k, el2) in row.iter().enumerate().skip(start_idx as usize) {
                    if el2.is_ascii_digit() {
                    } else {
                        end_idx = (k - 1) as i32;
                        reached_end = false;
                        break;
                    }
                }

                if end_idx != -1 {
                    j = (end_idx + 1) as usize;
                }

                if reached_end {
                    j = row.len();
                    end_idx = (j as i32) - 1;
                }
                // println!(
                //     "Row: {}, start index: {}, end index: {}",
                //     i, start_idx, end_idx
                // );

                for z in start_idx..end_idx + 1 {
                    if neighbour_has_special_symbol(matrix, i, z) {
                        res += (&matrix[i][(start_idx as usize)..(end_idx as usize) + 1])
                            .iter()
                            .collect::<String>()
                            .parse::<i32>()
                            .unwrap();

                        break;
                    }
                }
            } else {
                j += 1;
            }
        }
    }
    return res;
}

pub fn part2(matrix: &Vec<Vec<char>>) {}

fn display_matrix(matrix: &Vec<Vec<char>>) {
    for row in matrix {
        for &element in row {
            print!("{} ", element);
        }
        println!();
    }
}

pub fn run(mode: RunMode, part: Parts) {
    let fp;
    match part {
        Parts::Part1 => {
            match mode {
                RunMode::Example => fp = "./src/day3/example_part1.txt",
                RunMode::TestCase => fp = "./src/day3/test_part1.txt",
            }

            let mut row_count = 0;
            let mut col_count = 0;
            if let Ok(lines) = fs_util::read_lines(fp) {
                if let Some(Ok(first_line)) = lines.take(1).next() {
                    col_count = first_line.len();
                }
            }

            if let Ok(lines) = fs_util::read_lines(fp) {
                row_count = lines.count();
            }

            let mut matrix = vec![vec!['.'; col_count]; row_count];

            if let Ok(lines) = fs_util::read_lines(fp) {
                for (i, el) in lines.enumerate() {
                    let line = el.as_ref().unwrap();
                    for (j, ch) in line.chars().enumerate() {
                        matrix[i][j] = ch;
                    }
                }
            }

            println!("{}", part1(&matrix));
        }
        Parts::Part2 => {}
    }
}

use std::{default, error::Error, io::BufRead, ops::Index, u32, vec};

use crate::{fs_util, mode::RunMode, parts::Parts};

pub fn part1(input: &str) -> Vec<bool> {
    let max_red = 12;
    let max_blue = 14;
    let max_green = 13;
    let events: Vec<&str> = input.rsplit_once(":").unwrap().1.split(";").collect();
    let game_impossible_events: Vec<bool> = events
        .iter()
        .map(|&events| {
            // Outputs:
            // ["3 blue", "4 red"]
            let event_vec: Vec<&str> = events.split(",").collect::<Vec<&str>>();

            let event_vec_res = event_vec
                .iter()
                .map(|&event| {
                    let (count, color) = event.rsplit_once(" ").unwrap();
                    match color {
                        "blue" => count.trim().parse::<u32>().unwrap() <= max_blue,
                        "red" => count.trim().parse::<u32>().unwrap() <= max_red,
                        "green" => count.trim().parse::<u32>().unwrap() <= max_green,
                        _default => false,
                    }
                })
                .collect::<Vec<bool>>();

            // If atleast one `false` is present, that game is not possible
            return event_vec_res.contains(&false);
        })
        .collect();
    return game_impossible_events;
}

pub fn part2(input: &str) -> u32 {
    let mut max_blue = 1;
    let mut max_red = 1;
    let mut max_green = 1;

    let events_all: Vec<&str> = input.rsplit_once(":").unwrap().1.split(";").collect();
    for &events in events_all.iter() {
        // Outputs:
        // ["3 blue", "4 red"]
        let event_vec: Vec<&str> = events.split(",").collect::<Vec<&str>>();

        let _: Vec<()> = event_vec
            .iter()
            .map(|&event| {
                let (count, color) = event.rsplit_once(" ").unwrap();
                match color {
                    "blue" => {
                        let bcount = count.trim().parse::<u32>().unwrap();
                        if bcount > max_blue {
                            max_blue = bcount;
                        }
                    }
                    "red" => {
                        let rcount = count.trim().parse::<u32>().unwrap();
                        if rcount > max_red {
                            max_red = rcount;
                        }
                    }
                    "green" => {
                        let gcount = count.trim().parse::<u32>().unwrap();
                        if gcount > max_green {
                            max_green = gcount;
                        }
                    }
                    _default => (),
                }
            })
            .collect();
    }
    return max_blue * max_red * max_green;
}

pub fn run(mode: RunMode, part: Parts) {
    let fp;
    match part {
        Parts::Part1 => {
            match mode {
                RunMode::Example => fp = "./src/day2/example_part1.txt",
                RunMode::TestCase => fp = "./src/day2/test_part1.txt",
            }
            if let Ok(lines) = fs_util::read_lines(fp) {
                let mut res_count = 0;
                for (i, el) in lines.enumerate() {
                    let game_impossible_events: Vec<bool> = part1(el.as_ref().unwrap());
                    if !game_impossible_events.contains(&true) {
                        res_count += i + 1;
                    }
                }
                println!("Result : {}", res_count);
            }
        }
        Parts::Part2 => {
            match mode {
                RunMode::Example => fp = "./src/day2/example_part1.txt",
                RunMode::TestCase => fp = "./src/day2/test_part1.txt",
            }
            if let Ok(lines) = fs_util::read_lines(fp) {
                let mut res_count = 0;
                for (_, el) in lines.enumerate() {
                    let line = el.as_ref().unwrap();
                    let power: u32 = part2(line);
                    res_count += power;
                }
                println!("Result : {}", res_count);
            }
        }
    }
}
